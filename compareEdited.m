%close all; clear; clc ;
%%
tracker=load('Data/data1.txt');
tracker2=load('Data/data_raw_1.txt');

figure();
subplot 321
hold on;
plot(tracker(:,1),'r')
title('Position over X axis');
hold off;

subplot 323
hold on;
plot(tracker(:,2),'r')
title('Position over Y axis');
hold off;

subplot 325
hold on;
plot(tracker(:,3),'r')
title('Position over Z axis');
hold off;

subplot 322
hold on;
plot(tracker(:,4),'r')
title('Orientation over X axis');
hold off;

subplot 324
hold on;
plot(tracker(:,5),'r')
title('Orientation over Y axis');
hold off;

subplot 326
hold on;
plot(tracker(:,6),'r')
title('Orientation over Z axis');
hold off;

% Raw data

figure();
subplot 331
hold on;
plot(tracker2(:,1),'r')
title('gyro1 X');
hold off;

subplot 332
hold on;
plot(tracker2(:,2),'r')
title('gyro1 Y');
hold off;

subplot 333
hold on;
plot(tracker2(:,3),'r')
title('gyro1 Z');
hold off;

subplot 334
hold on;
plot(tracker2(:,4),'r')
title('acc1 X');
hold off;

subplot 335
hold on;
plot(tracker2(:,5),'r')
title('acc1 Y');
hold off;

subplot 336
hold on;
plot(tracker2(:,6),'r')
title('acc1 Z');
hold off;

subplot 337
hold on;
plot(tracker2(:,7),'r')
title('mag1 X');
hold off;

subplot 338
hold on;
plot(tracker2(:,8),'r')
title('mag1 Y');
hold off;

subplot 339
hold on;
plot(tracker2(:,9),'r')
title('mag1 Z');
hold off;

figure();
subplot 331
hold on;
plot(tracker2(:,10),'r')
title('gyro2 X');
hold off;

subplot 332
hold on;
plot(tracker2(:,11),'r')
title('gyro2 Y');
hold off;

subplot 333
hold on;
plot(tracker2(:,12),'r')
title('gyro2 Z');
hold off;

subplot 334
hold on;
plot(tracker2(:,13),'r')
title('acc2 X');
hold off;

subplot 335
hold on;
plot(tracker2(:,14),'r')
title('acc2 Y');
hold off;

subplot 336
hold on;
plot(tracker2(:,15),'r')
title('acc2 Z');
hold off;

subplot 337
hold on;
plot(tracker2(:,16),'r')
title('mag2 X');
hold off;

subplot 338
hold on;
plot(tracker2(:,17),'r')
title('mag2 Y');
hold off;

subplot 339
hold on;
plot(tracker2(:,18),'r')
title('mag2 Z');
hold off;

figure()
plot(tracker2(:,19),'r')
title('elbow angle');