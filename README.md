### 1) Folder "Data" includes subfolders S1 (subject 1) and S2 (subject 2).
   Each folder contains folder with different actions, e.g. PickNPlace_Down etc.
   Each action contains data files with two different data formats.
      
      1. data#.txt - each row contains information about end effector's position in space (XYZ) and orientation in 
         degrees (Pitch-Yaw-Roll). Total six numbers per row. The whole file corresponds to one complete action
      2. data_raw_#.txt - each row contains raw information received from two IMU sensors and potentiometer. First
         nine numbers correspond to the wrist IMU: gyro_x, gyro_y, gyro_z, acc_x, acc_y, acc_z, magn_x, magn_y, magn_z.
         Second nine numbers correspond to shoulder IMU (ordered is the same as wrist IMU). Finally, last number is 
         the elbow angle in radians.
    
### 2) Folder "End Effector Only" has information only about end effector's position (XYZ) and orientation in degrees (Pitch-Yaw-Roll)